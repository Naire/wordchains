package com.sabre.task.word.chains.dictionary;

import static org.junit.Assert.fail;

import com.sabre.task.word.chains.exceptions.DictionaryNotFoundException;
import com.sabre.task.word.chains.service.Dictionary;
import com.sabre.task.word.chains.service.DictionaryService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import lombok.extern.slf4j.Slf4j;


@Slf4j
public class DictionaryTest {

  private static final String fileName = "testWordList.txt";
  private static final String testWordsDifferentByOne = "testWordsDifferentByOne.txt";
  private static final String DEBUG_MSG = "Test time: %dms";

  private long start;
  private long end;

  @Before
  public void startTheClock() {
    start = System.currentTimeMillis();
  }

  @After
  public void stopTheClock() {
    end = System.currentTimeMillis();
    log.debug(String.format(DEBUG_MSG, (end - start)));
  }

  @Test
  public void shouldReadFileIntoADictionary() throws DictionaryNotFoundException {
    final Dictionary sut = new DictionaryService(fileName);
    final List<String> wordList = sut.getWordList();
    final File file = new File(
        getClass().getClassLoader().getResource(fileName).getFile());
    try (Scanner scanner = new Scanner(file)) {
      int i = 0;
      while (scanner.hasNextLine()) {
        Assert.assertEquals(scanner.nextLine(), wordList.get(i));
        i++;
      }
    } catch (FileNotFoundException e) {
      fail(e.getMessage());
    }
  }

  @Test(expected = DictionaryNotFoundException.class)
  public void shouldThrowExceptionIfFileNotFound() throws DictionaryNotFoundException {
    new DictionaryService("unknown");
  }

  @Test
  public void shouldReturnTrueForValidWord() throws DictionaryNotFoundException {
    final Dictionary sut = new DictionaryService(fileName);
    final String validWord = "connotation";
    Assert.assertTrue(sut.containsWord(validWord));
  }

  @Test
  public void shouldReturnFalseForInvalidWord() throws DictionaryNotFoundException {
    final Dictionary sut = new DictionaryService(fileName);
    final String invalidWord = "thisisinvalid";
    Assert.assertFalse(sut.containsWord(invalidWord));
  }

  @Test
  public void givenWordShouldGenerateAllValidWordsDifferingByOne()
      throws DictionaryNotFoundException {
    final Dictionary sut = new DictionaryService(testWordsDifferentByOne);
    Set<String> expected = new HashSet<>();
    expected.add("bat");
    expected.add("can");
    expected.add("cot");
    Assert.assertEquals(expected, sut.getWordsDifferingByLetter("cat"));
  }

  @Test
  public void givenInvalidWordShouldReturnEmptySet() throws DictionaryNotFoundException {
    final Dictionary sut = new DictionaryService(fileName);
    final String invalidWord = "thisisinvalid";
    Assert.assertEquals(Collections.emptySet(), sut.getWordsDifferingByLetter(invalidWord));
  }

}
