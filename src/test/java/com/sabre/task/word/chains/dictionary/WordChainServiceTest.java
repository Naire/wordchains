package com.sabre.task.word.chains.dictionary;

import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.sabre.task.word.chains.exceptions.InvalidWordException;
import com.sabre.task.word.chains.service.Dictionary;
import com.sabre.task.word.chains.service.GraphService;
import com.sabre.task.word.chains.service.WordChainService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
public class WordChainServiceTest {
  private static final List<String> wordList =
      Arrays.asList("code", "robe", "robs", "rode", "rods", "rube", "rubs", "ruby");
  private static final List<String> invalidWordList =
      Arrays.asList("invalidcode", "invalidrobe", "invalidrobs", "invalidrode", "invalidrods",
          "invalidrube", "invalidrubs", "invalidruby");

  private WordChainService sut;
  @Mock
  private GraphService graphService;
  @Mock
  private Dictionary dictionary;

  @Before
  public void initMocks() {
    when(dictionary.getWordList()).thenReturn(wordList);
    when(dictionary.containsWord(Mockito.argThat(invalidWordList::contains))).thenReturn(false);
    when(dictionary.containsWord(Mockito.argThat(wordList::contains))).thenReturn(true);
    when(graphService.getShortestPath(anyString(), anyString())).thenReturn(anyList());
    sut = new WordChainService(dictionary, graphService);
  }

  @Test(expected = InvalidWordException.class)
  public void givenInvalidWordsShouldThrowException() throws InvalidWordException {
    sut.getWordChain(invalidWordList.get(0), invalidWordList.get(1));
    verify(graphService, never()).getShortestPath(anyString(), anyString());
  }

  @Test
  public void givenValidWordsShouldReturnList() throws InvalidWordException {
    sut.getWordChain(wordList.get(0), wordList.get(1));
    verify(dictionary, times(2)).containsWord(anyString());
    verify(graphService, times(1)).getShortestPath(wordList.get(0), wordList.get(1));
  }
}
