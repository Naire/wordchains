package com.sabre.task.word.chains.dictionary;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import com.sabre.task.word.chains.model.Node;
import com.sabre.task.word.chains.repository.DijkstraRepository;
import com.sabre.task.word.chains.repository.PathRepository;
import com.sabre.task.word.chains.service.Dictionary;
import com.sabre.task.word.chains.service.GraphService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
public class GraphServiceTest {
  private GraphService sut;
  @Mock
  private Dictionary dictionary;
  @Mock
  private PathRepository pathRepository;
  @Mock
  private DijkstraRepository dijkstraRepository;

  private static final String DEBUG_MSG = "Test time: %dms";
  private final List<String> wordList =
      Arrays.asList("code", "robe", "robs", "rode", "rods", "rube", "rubs", "ruby");

  private int nodeCount;
  private long start;
  private Map<String, Node> expected;

  @Before
  public void initMocksStartTheClock() {
    expected = createExpectedSet();
    when(dictionary.getWordList()).thenReturn(wordList);
    when(dijkstraRepository.getDijkstraMap(anyInt())).thenReturn(Collections.emptyMap());
    when(pathRepository.getPath(anyString(), anyString())).thenReturn(Collections.emptyList());
    start = System.currentTimeMillis();
    sut = new GraphService(dictionary, pathRepository, dijkstraRepository);
  }

  @After
  public void stopTheClock() {
    long end = System.currentTimeMillis();
    log.info(String.format(DEBUG_MSG, (end - start)));
  }

  @Test
  public void shouldGenerateGraphFromDictionary() {
    Map<String, Node> actualNodes = sut.getNodes();
    Assert.assertEquals(expected, actualNodes);
  }

  @Test
  public void givenTwoValidWordsShouldReturnShortestPath() {
    String start = "ruby";
    String stop = "code";
    List<String> expected = new LinkedList<>();
    //another, longer possibility is: [ruby, rube, rubs, robe, robs, rode, rods, code]
    expected.add("ruby");
    expected.add("rube");
    expected.add("robe");
    expected.add("rode");
    expected.add("code");

    Assert.assertEquals(expected, sut.getShortestPath(start, stop));
  }


  public Map<String, Node> createExpectedSet() {
    nodeCount = 0;
    return wordList.stream()
        .map(Node::new)
        .map(node -> getNeighbours(node.getWord()))
        .map(node -> {
          node.setNumber(nodeCount);
          nodeCount++;
          return node;
        })
        .collect(Collectors.toMap(Node::getWord, node -> node));
  }

  /* LIST: "code", "robe", "robs", "rode", "rods", "rube", "rubs", "ruby"
   * code: rode
   * robe: robs, rode, rube
   * robs: robe, rods, rubs
   * rode: code, robe, rods
   * rods: robs, rode
   * rube: rubs, ruby, robe
   * rubs: rube, ruby, robs
   * ruby: rubs, rube
   * */
  private Node getNeighbours(String word) {
    Node node = new Node(word);
    Set<String> neighbours = new HashSet<>();
    switch (word) {
      case "code": {
        neighbours.add("rode");
        break;
      }
      case "robe": {
        neighbours.add("robs");
        neighbours.add("rode");
        neighbours.add("rube");
        break;
      }
      case "robs": {
        neighbours.add("robe");
        neighbours.add("rods");
        neighbours.add("rubs");
        break;
      }
      case "rode": {
        neighbours.add("code");
        neighbours.add("robe");
        neighbours.add("rods");
        break;
      }
      case "rods": {
        neighbours.add("robs");
        neighbours.add("rode");
        break;
      }
      case "rube": {
        neighbours.add("rubs");
        neighbours.add("ruby");
        neighbours.add("robe");
        break;
      }
      case "rubs": {
        neighbours.add("rube");
        neighbours.add("ruby");
        neighbours.add("robs");
        break;
      }
      case "ruby": {
        neighbours.add("rubs");
        neighbours.add("rube");
        break;
      }
    }

    neighbours.forEach(neighbour -> node.addNeighbour(new Node(neighbour)));
    when(dictionary.getWordsDifferingByLetter(word)).thenReturn(neighbours);
    return node;
  }

}
