package com.sabre.task.word.chains.dictionary;

import com.sabre.task.word.chains.service.GraphService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootTest
@RunWith(SpringRunner.class)
//due to being a bit slow this test is excluded from normal execution
//to enable it remove configuration from maven-compiler-plugin in the pom.xml
//or run manually from IDE
public class GraphDictionaryIntegrationTest {
  private static final String DEBUG_MSG = "Test time: %dms";

  private long start;
  private long end;

  @Autowired
  private GraphService sut;


  @Before
  public void startTheClock() {
    start = System.currentTimeMillis();
  }

  @After
  public void stopTheClock() {
    end = System.currentTimeMillis();
    log.info(String.format(DEBUG_MSG, (end - start)));
  }

  @Test
  public void givenRubyCodeShouldReturnShortestPath() {
    String start = "ruby";
    String stop = "code";
    List<String> expected = new ArrayList<>();
    expected.add("ruby");
    expected.add("rube");
    expected.add("rude");
    expected.add("rode");
    expected.add("code");

    Assert.assertEquals(expected, sut.getShortestPath(start, stop));
  }

  @Test
  public void givenCatDogShouldReturnShortestPath() {
    String start = "cat";
    String stop = "dog";
    List<String> expected = new ArrayList<>();
    expected.add("cat");
    expected.add("cot");
    expected.add("cog");
    expected.add("dog");

    Assert.assertEquals(expected, sut.getShortestPath(start, stop));
  }

  @Test
  public void givenCatCogShouldReturnShortestPath() {
    String start = "cat";
    String stop = "cog";
    List<String> expected = new ArrayList<>();
    expected.add("cat");
    expected.add("cot");
    expected.add("cog");

    Assert.assertEquals(expected, sut.getShortestPath(start, stop));
  }

  @Test
  public void givenLeadGoldShouldReturnShortestPath() {
    String start = "lead";
    String stop = "gold";
    List<String> expected = new ArrayList<>();
    expected.add("lead");
    expected.add("load");
    expected.add("goad");
    expected.add("gold");

    Assert.assertEquals(expected, sut.getShortestPath(start, stop));
  }
}
