package com.sabre.task.word.chains.repository;

import com.sabre.task.word.chains.helper.WordDistance;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Component
public class DijkstraRepository {
  private final Map<Integer, Map<String, WordDistance>> repository = new HashMap<>();

  public void addDijkstraMap(int nodeNumber, Map<String, WordDistance> dijkstraMap) {
    repository.put(nodeNumber, dijkstraMap);
  }

  public Map<String, WordDistance> getDijkstraMap(int nodeNumber) {
    return repository.getOrDefault(nodeNumber, Collections.emptyMap());
  }

}
