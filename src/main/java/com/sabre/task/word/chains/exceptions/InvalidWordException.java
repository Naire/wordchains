package com.sabre.task.word.chains.exceptions;

import org.springframework.http.HttpStatus;

public class InvalidWordException extends ExceptionWithHttpStatus {
  private static final String FORMATTED_MSG = "Word: %s has not been found in dictionary.";
  private static final String WORD_LENGTH =
      "Words should be of equal length. First word has length: %s while second word has length: %s";

  public InvalidWordException(String word) {
    super(String.format(FORMATTED_MSG, word));
    setHttpCode();
  }

  public InvalidWordException(int word1Length, int word2Length) {
    super(String.format(WORD_LENGTH, word1Length, word2Length));
    setHttpCode();
  }

  private void setHttpCode() {
    setHttpStatusCode(HttpStatus.UNPROCESSABLE_ENTITY);
  }
}
