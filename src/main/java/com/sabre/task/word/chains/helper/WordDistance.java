package com.sabre.task.word.chains.helper;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class WordDistance implements Comparable {
  private String word;
  private int howFar;
  private String previous;

  @Override
  public int compareTo(Object word) {
    final String className = this.getClass().getName();
    if (word instanceof WordDistance &&
        className.equals(word.getClass().getName())) {
      return Integer.compare(getHowFar(), ((WordDistance) word).getHowFar());
    }
    throw new ClassCastException("Cannot compare objects that are not of class " + className);
  }
}
