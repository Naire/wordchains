package com.sabre.task.word.chains.exceptions;

import org.springframework.http.HttpStatus;

public class DictionaryNotFoundException extends ExceptionWithHttpStatus {

  private static final String DEFAULT_MSG = "Dictionary has not been found.";
  private static final String FORMATTED_MSG = "Dictionary with name: %s has not been found in directory: %s.";

  public DictionaryNotFoundException() {
    this(DEFAULT_MSG);
  }

  public DictionaryNotFoundException(String fileName, String directory) {
    this(String.format(FORMATTED_MSG, fileName, directory));
  }

  public DictionaryNotFoundException(String message) {
    super(message);
    setHttpStatusCode(HttpStatus.NOT_FOUND);
  }
}