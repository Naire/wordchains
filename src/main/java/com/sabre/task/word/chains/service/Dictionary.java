package com.sabre.task.word.chains.service;

import com.sabre.task.word.chains.exceptions.DictionaryNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public abstract class Dictionary {

  private static final String dictionaryFileName = "wordlist.txt";

  @Getter
  private List<String> wordList = new ArrayList<>();
  @Getter
  private Map<Integer, Set<String>> wordsByLengthMap = new HashMap<>();

  public Dictionary() throws DictionaryNotFoundException {
    init(dictionaryFileName);
  }

  /**
   * @param resourcesFileName name of a file in the resources directory (on a classpath). Max number
   * of words/lines cannot be bigger than MAX_INTEGER. This file have to be a dictionary - sorted
   * alphabetically.
   */
  public Dictionary(String resourcesFileName) throws DictionaryNotFoundException {
    init(resourcesFileName);
  }

  public abstract boolean containsWord(String word);

  public abstract Set<String> getWordsDifferingByLetter(String originalWord);


  private void init(String dictionaryFileName) throws DictionaryNotFoundException {
    try {
      ClassLoader cl = this.getClass().getClassLoader();
      InputStream inputStream = cl.getResourceAsStream(dictionaryFileName);
      wordList = IOUtils.readLines(inputStream, StandardCharsets.UTF_8).stream()
          .filter(line -> !line.isEmpty())
          .map(String::trim)
          .map(String::toLowerCase)
          .peek(word -> {
            int len = word.length();
            Set<String> strings;
            if(wordsByLengthMap.containsKey(len)) {
              strings = wordsByLengthMap.get(len);
              strings.add(word);
              wordsByLengthMap.put(len, strings);
            } else {
              strings = new TreeSet<>();
              strings.add(word);
            }
            wordsByLengthMap.put(len, strings);
          })
          .collect(Collectors.toList());
    } catch (IOException | NullPointerException e) {
      log.error(String
          .format("Unable to initialize dictionary with filename: %s.", e));
      throw new DictionaryNotFoundException(dictionaryFileName, "resources");
    }
  }


}
