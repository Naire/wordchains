package com.sabre.task.word.chains.exceptions;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Getter
@Setter
public abstract class ExceptionWithHttpStatus extends Exception {

  private HttpStatus httpStatusCode = HttpStatus.INTERNAL_SERVER_ERROR;
  private static final String DEFAULT_MSG = "Unknown error has occurred.";

  public ExceptionWithHttpStatus() {
    super(DEFAULT_MSG);
  }

  public ExceptionWithHttpStatus(final String message) {
    super(message);
  }


}
