package com.sabre.task.word.chains.service;

import com.sabre.task.word.chains.helper.WordDistance;
import com.sabre.task.word.chains.model.Node;
import com.sabre.task.word.chains.repository.DijkstraRepository;
import com.sabre.task.word.chains.repository.PathRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.Getter;


/**
 * Unweighted, undirected graph. Nodes are words, which exist in the Dictionary provided in the
 * constructor. Edges are created between words which differ by 1 letter and are of the same length.
 * Uses Dijkstra algorithm to calculate the shortest path between words. Graph initialization takes
 * a long time - should be done only once per dictionary!
 */
@Service
public class GraphService {

  @Getter
  private Map<String, Node> nodes;

  private Dijkstra dijkstra;
  private Dictionary dictionary;
  private PathRepository repository;
  private DijkstraRepository dijkstraRepository;
  private int nodeCounter;

  @Autowired
  public GraphService(Dictionary dictionary, PathRepository repository,
                      DijkstraRepository dijkstraRepository) {
    this.nodes = new HashMap<>();
    this.dictionary = dictionary;
    this.repository = repository;
    this.dijkstraRepository = dijkstraRepository;
    initGraph();
  }

  /**
   * Uses Dijkstra algorithm.
   * @param start first word on the path.
   * @param stop last word on the path.
   * @return a list of strings representing the shortest path between start and stop or empty list
   * if path does not exist.
   */
  public List<String> getShortestPath(String start, String stop) {
    if (repository != null) {
      final List<String> path = repository.getPath(start, stop);
      if (!path.isEmpty()) {
        return path;
      }
    }
    final Map<String, WordDistance>
        dijkstraMap = dijkstra.getDijkstraMapFor(start);
    return dijkstra.isReachable(dijkstraMap, stop) ? dijkstra
        .calculateShortestPath(dijkstraMap, start, stop)
        : Collections.emptyList();
  }

  private void initGraph() {
    nodeCounter = 0;
    dictionary.getWordList().stream()
        .map(this::getNode)
        .forEach(node -> nodes.put(node.getWord(), node));
    dijkstra = new Dijkstra(nodes, repository, dijkstraRepository);
  }

  private Node getNode(String word) {
    final Set<Node> neighbours = dictionary.getWordsDifferingByLetter(word).stream()
        .map(Node::new)
        .collect(Collectors.toSet());
    return new Node(nodeCounter++, word, neighbours);
  }
}
