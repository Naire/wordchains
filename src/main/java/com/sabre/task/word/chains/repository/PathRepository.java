package com.sabre.task.word.chains.repository;

import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;

@Component
public class PathRepository {
  private final Map<String, List<String>> repository;

  public PathRepository() {
    repository = new HashMap<>();
  }

  /**
   * Repository for already computed paths between 'start' and 'stop' words. If the key already
   * exists it will overwrite.
   * @param startStopKey if 'start' word is: ruby and 'stop' word is: code then the key should look
   * like: ruby-code.
   * @param path a computed, shortest path between 'start' and 'stop' words - the word chain.
   */
  public void addPath(String startStopKey, List<String> path) {
    repository.put(startStopKey, path);
  }

  public List<String> getPath(String start, String stop) {
    return repository.getOrDefault(start + "-" + stop, Collections.emptyList());
  }
}
