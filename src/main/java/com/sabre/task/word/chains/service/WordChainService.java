package com.sabre.task.word.chains.service;

import com.sabre.task.word.chains.exceptions.InvalidWordException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WordChainService {
  private Dictionary dictionary;
  private GraphService graphService;

  @Autowired
  public WordChainService(Dictionary dictionary,
                          GraphService graphService) {
    this.dictionary = dictionary;
    this.graphService = graphService;
  }

  /**
   * There’s a type of puzzle where the challenge is to build a chain of words, starting with one
   * particular word and ending with another. Successive entries in the chain must all be real
   * words, and each can differ from the previous word by just one letter.
   * @param start a valid word (which exists in the dictionary) - first word of a chain
   * @param stop a valid word (which exists in the dictionary) - last word of a chain
   * @return linked list of words - a chain which elements differ by one letter or empty list if not
   * possible to transform 'start' word into the 'stop' word.
   * @throws InvalidWordException if either 'start' or 'stop' word are not valid (don't exist in the
   * dictionary).
   */
  public List<String> getWordChain(String start, String stop) throws InvalidWordException {
    final int startLen = start.length();
    final int stopLen = stop.length();
    if (startLen != stopLen) {
      throw new InvalidWordException(startLen, stopLen);
    }
    if (!dictionary.containsWord(start)) {
      throw new InvalidWordException(start);
    }
    if (!dictionary.containsWord(stop)) {
      throw new InvalidWordException(stop);
    }
    return graphService.getShortestPath(start, stop);
  }
}
