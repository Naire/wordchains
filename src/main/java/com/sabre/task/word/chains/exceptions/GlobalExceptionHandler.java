package com.sabre.task.word.chains.exceptions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {

  @ExceptionHandler({ExceptionWithHttpStatus.class})
  public final String handleException(Exception ex, Model model) {
    if (ex instanceof ExceptionWithHttpStatus) {
      model.addAttribute("message", ex.getMessage());
      model.addAttribute("status", ((ExceptionWithHttpStatus) ex).getHttpStatusCode());
      return "error";
    }
    log.error(
        "An error has occurred: " + ex.getClass().getName() + ", message: " + ex.getMessage(), ex);

    model.addAttribute("message",
        "Unknown error has occurred. Exception message: " + ex.getMessage());
    model.addAttribute("status", HttpStatus.INTERNAL_SERVER_ERROR);
    return "error";
  }
}
