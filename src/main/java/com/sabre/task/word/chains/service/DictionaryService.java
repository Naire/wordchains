package com.sabre.task.word.chains.service;

import com.sabre.task.word.chains.exceptions.DictionaryNotFoundException;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class DictionaryService extends Dictionary {

  public DictionaryService() throws DictionaryNotFoundException {
  }

  public DictionaryService(String resourcesFileName) throws DictionaryNotFoundException {
    super(resourcesFileName);
  }

  /**
   * Checks word validity.
   * @param word String to check
   * @return true if given word exists in this dictionary, false otherwise.
   */
  @Override
  public boolean containsWord(String word) {
    return getWordsByLengthMap().containsKey(word.length()) && getWordsByLengthMap().get(word.length()).contains(word);
  }

  /**
   * Get set of valid (according to this dictionary) words which differ by 1 letter from the given
   * word.
   * @param originalWord word which should be valid (exist in this dictionary).
   * @return a set of words different by 1 letter from the given word. Set can be empty if there are
   * no such words.
   */
  @Override
  public Set<String> getWordsDifferingByLetter(String originalWord) {
    return getWordsByLengthMap().get(originalWord.length()).stream()
        .filter(word -> isDifferentByOneLetter(originalWord, word))
        .collect(Collectors.toSet());
  }

  private boolean isDifferentByOneLetter(String first, String second) {
    boolean alreadyDiffers = false;
    for (int i = 0; i < first.length(); i++) {
      if ((first.charAt(i) != second.charAt(i))) {
        //words already differ by one character so they are different by more than one character
        if (alreadyDiffers) {
          return false;
        }
        alreadyDiffers = true;
      }
    }
    return alreadyDiffers;
  }
}
