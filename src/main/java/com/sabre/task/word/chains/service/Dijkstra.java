package com.sabre.task.word.chains.service;

import com.sabre.task.word.chains.helper.WordDistance;
import com.sabre.task.word.chains.model.Node;
import com.sabre.task.word.chains.repository.DijkstraRepository;
import com.sabre.task.word.chains.repository.PathRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;

public class Dijkstra {
  private static final String EMPTY_STRING = "";
  private static final String PATH_REPO_KEY = "%s-%s";
  private static final int EDGE_WEIGHT = 1;
  private Map<String, Node> nodes;
  private Map<Integer, String> nodeNumberToWord;

  private PathRepository pathRepository;
  private DijkstraRepository dijkstraRepository;


  public Dijkstra(Map<String, Node> nodes, PathRepository pathRepository,
                  DijkstraRepository dijkstraRepository) {
    this.nodes = nodes;
    this.pathRepository = pathRepository;
    this.dijkstraRepository = dijkstraRepository;
    nodeNumberToWord = new HashMap<>();
    for (Map.Entry<String, Node> entry : nodes.entrySet()) {
      nodeNumberToWord.put(entry.getValue().getNumber(), entry.getKey());
    }
  }

  public boolean isReachable(Map<String, WordDistance> dijkstraMap, String stop) {
    return dijkstraMap.keySet().contains(stop) && !EMPTY_STRING
        .equals(dijkstraMap.get(stop).getPrevious());
  }

  public List<String> calculateShortestPath(Map<String, WordDistance> dijkstraMap, String start,
                                            String stop) {
    List<String> shortestPath = new ArrayList<>();
    shortestPath.add(stop);
    String previous = dijkstraMap.get(stop).getPrevious();
    while (!previous.equals(EMPTY_STRING)) {
      shortestPath.add(previous);
      previous = dijkstraMap.get(previous).getPrevious();
    }
    String repoKey = String.format(PATH_REPO_KEY, stop, start);
    if (pathRepository != null) {
      pathRepository.addPath(repoKey, shortestPath);
    }
    Collections.reverse(shortestPath);
    repoKey = String.format(PATH_REPO_KEY, start, stop);
    if (pathRepository != null) {
      pathRepository.addPath(repoKey, shortestPath);
    }
    return shortestPath;
  }

  public Map<String, WordDistance> getDijkstraMapFor(String startWord) {
    if (dijkstraRepository != null) {
      final Map<String, WordDistance> dijkstraMap =
          dijkstraRepository.getDijkstraMap(nodes.get(startWord).getNumber());
      if (!dijkstraMap.isEmpty()) {
        return dijkstraMap;
      }
    }
    boolean[] visited = new boolean[nodes.size()];
    int[] distances = new int[nodes.size()];
    String[] previous = new String[nodes.size()];
    for (int j = 0; j < nodes.size(); j++) {
      distances[j] = Integer.MAX_VALUE - 1;
      previous[j] = EMPTY_STRING;
    }
    Queue<WordDistance> queue = new PriorityQueue<>();
    queue.add(new WordDistance(startWord, 0, EMPTY_STRING));
    int i = nodes.get(startWord).getNumber();
    distances[i] = 0;
    while (queue.size() != 0) {
      final String currentWord = queue.poll().getWord();
      i = nodes.get(currentWord).getNumber();
      if (!visited[i]) {
        visited[i] = true;
      }

      for (Node node : nodes.get(currentWord).getNeighbours()) {
        if (!visited[node.getNumber()]) {
          String neighbourWord = node.getWord();
          final int neighbourNumber = nodes.get(neighbourWord).getNumber();
          int distance = distances[neighbourNumber];
          final int newDistance = distances[nodes.get(currentWord).getNumber()] + EDGE_WEIGHT;
          if (distance > newDistance) {
            queue.remove(new WordDistance(neighbourWord, distance, previous[neighbourNumber]));

            distances[neighbourNumber] = newDistance;
            previous[neighbourNumber] = currentWord;

            queue.add(
                new WordDistance(neighbourWord, distances[neighbourNumber],
                    previous[neighbourNumber]));
          }
        }
      }
    }
    return createDijkstraMap(startWord, distances, previous);
  }

  private Map<String, WordDistance> createDijkstraMap(String startWord, int[] distances,
                                                      String[] previous) {
    Map<String, WordDistance> dijkstra = new HashMap<>(nodes.size());
    for (int i = 0; i < nodes.size(); i++) {
      final String word = nodeNumberToWord.get(i);
      dijkstra.put(word, new WordDistance(word, distances[i], previous[i]));
    }
    if (dijkstraRepository != null) {
      dijkstraRepository.addDijkstraMap(nodes.get(startWord).getNumber(), dijkstra);
    }
    return dijkstra;
  }

}
