package com.sabre.task.word.chains.model;

import java.util.HashSet;
import java.util.Set;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Node {
  private int number;
  private String word;
  private Set<Node> neighbours;

  public Node(String word) {
    this.word = word;
    neighbours = new HashSet<>();
  }

  public Node(int number, String word, Set<Node> neighbours) {
    this.number = number;
    this.word = word;
    this.neighbours = neighbours;
  }

  public boolean addNeighbour(Node node) {
    return neighbours.add(node);
  }
}
