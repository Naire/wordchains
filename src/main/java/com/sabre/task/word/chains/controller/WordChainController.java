package com.sabre.task.word.chains.controller;

import com.sabre.task.word.chains.exceptions.InvalidWordException;
import com.sabre.task.word.chains.service.WordChainService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(value = "/")
public class WordChainController {

  @Autowired
  private WordChainService service;

  @RequestMapping(method = RequestMethod.GET)
  public String getWelcomeMessage() {
    return "welcome";
  }

  @RequestMapping(value = "/wordchain", method = RequestMethod.POST)
  public String getWordChain(@RequestParam String start, @RequestParam String stop,
      Model model)
      throws InvalidWordException {
    final List<String> wordChain = service.getWordChain(start.toLowerCase(), stop.toLowerCase());
    model.addAttribute("start", start);
    model.addAttribute("stop", stop);
    if (wordChain.isEmpty()) {
      return "notReachable";
    }

    model.addAttribute("wordChainList", wordChain);
    return "wordchain";
  }
}
